# -*- encoding: utf-8 -*-

Gem::Specification.new do |s|
  s.name = "rubytree"
  s.version = "0.5.3"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.authors = ["Anupam Sengupta"]
  s.date = "2010-01-01"
  s.description = "Rubytree is a Ruby implementation of the generic Tree data structure.  It provides a generic tree data-structure with\nability to store keyed node-elements in the tree.  This implementation is node-centric, where the individual nodes on\nthe tree are the primary objects and form the leafs of the structure.  The implementation mixes in the Enumerable\nmodule."
  s.email = ["anupamsg@gmail.com"]
  s.extra_rdoc_files = ["History.txt", "Manifest.txt", "README", "COPYING", "ChangeLog"]
  s.files = ["History.txt", "Manifest.txt", "README", "COPYING", "ChangeLog"]
  s.homepage = "http://rubytree.rubyforge.org"
  s.rdoc_options = ["--main", "README"]
  s.require_paths = ["lib"]
  s.rubyforge_project = "rubytree"
  s.rubygems_version = "1.8.15"
  s.summary = "Rubytree is a Ruby implementation of the generic Tree data structure"

  if s.respond_to? :specification_version then
    s.specification_version = 3

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_development_dependency(%q<hoe>, [">= 2.4.0"])
    else
      s.add_dependency(%q<hoe>, [">= 2.4.0"])
    end
  else
    s.add_dependency(%q<hoe>, [">= 2.4.0"])
  end
end
